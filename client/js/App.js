'use strict';
require('../sass/app.scss');
require('./autocomplete.component.js');

const autocompleteEl = document.getElementById('autocomplete');
autocompleteEl.addEventListener('complete', ($event) => {
  const result = document.getElementById('autocomplete-result');
  result.innerHTML = JSON.stringify($event.detail)
});
