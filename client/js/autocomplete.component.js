'use strict';
require('./autocomplete.component.scss');

class AutocompleteComponent extends HTMLElement {

  constructor() {
    super();
    this.currentIndex = 0;
    this.scrollPositionToDown = 0;
    this.scrollPositionToUp = 0;
    this.consult = true;
    this.states = [];
    this.selected = {};

    this.autocompleteWrapperEl = this.wrapperElementCreation();
    this.autocompleteInputEl = this.inputElementCreation();
    this.autocompleteClearEl = this.inputClearIconElementCreation();
    this.autocompleteDatalistEl = this.datalistElementCreation();

    this.autocompleteWrapperEl.appendChild(this.autocompleteInputEl);
    this.autocompleteWrapperEl.appendChild(this.autocompleteClearEl);
    this.autocompleteWrapperEl.appendChild(this.autocompleteDatalistEl);

    this.appendChild(this.autocompleteWrapperEl);
  }

  //#region Wrapper Element Creation
  wrapperElementCreation() {
    const element = document.createElement('div');
    element.classList.add('autocomplete-wrapper');
    element.addEventListener('keydown', ($event) => this.wrapperOnKeyDownCallback($event));
    element.addEventListener('blur', () => this.datalistCleanOptions(this.selected));
    return element;
  }
  wrapperOnKeyDownCallback($event) {
    if (this.autocompleteDatalistEl.children.length === 0) {
      return;
    }

    let arrowPressedFn;
    switch ($event.key.toLowerCase()) {
      case 'tab':
      case 'enter':
        if (this.currentIndex != -1) {
          this.datalistItemOnClickCallback(this.states[this.currentIndex]);
        }
        return;
      case 'arrowup':
        arrowPressedFn = this.wrapperOnKeyDownCallbackCaseUp();
        break;
      case 'arrowdown':
        arrowPressedFn = this.wrapperOnKeyDownCallbackCaseDown();
        break;
      default: return;
    }
    this.wrapperOnArrowPressedCallback(arrowPressedFn);
  }
  wrapperOnKeyDownCallbackCaseUp() {
    this.scrollPositionToDown = 0;
    this.scrollPositionToUp++;
    return () => {
      this.currentIndex--;
      if (this.currentIndex < 0) {
        this.currentIndex = 0;
      }
      this.autocompleteDatalistEl.scrollBy(0, -36);
    };
  }
  wrapperOnKeyDownCallbackCaseDown() {
    this.scrollPositionToUp = 0;
    this.scrollPositionToDown++;
    return () => {
      this.scrollPositionToUp = 0;
      this.currentIndex++;
      if (this.currentIndex === this.autocompleteDatalistEl.children.length) {
        this.currentIndex--;
      }
      if (this.currentIndex >= 4) {
        this.autocompleteDatalistEl.scrollBy(0, 36);
      }
    };
  }
  wrapperOnArrowPressedCallback(fn) {
    this.consult = false;
    this.datalistItemCleanAllHover();
    fn();
    const currentItemEl = this.autocompleteDatalistEl.children[this.currentIndex];
    currentItemEl.classList.add('hover');
    this.autocompleteInputEl.value = this.states[this.currentIndex].name;
    this.consult = true;
  }
  //#endregion

  //#region Autocomplete Input Element Creation
  inputElementCreation() {
    const element = document.createElement('input');
    element.setAttribute('placeholder', 'Search State');
    element.classList.add('autocomplete-input');
    element.addEventListener('input', ($event) => this.inputOnInputCallback($event));
    element.addEventListener('focus', ($event) => {
      if ($event.target.value !== '') {
        this.inputOnInputCallback($event)
      }
    });
    return element;
  }
  inputOnInputCallback($event) {
    if (this.consult) {
      const query = $event.srcElement.value
      this.autocompleteWrapperEl.classList.add('not-empty');
      if (query === '') {
        this.datalistCleanOptions();
        this.autocompleteWrapperEl.classList.remove('not-empty');
        return;
      }

      fetch(`http://localhost:3000/api/states?term=${query}`, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json'
        }
      }).then(response => response.json())
        .then(states => {
          this.datalistCleanOptions();
          if (states.count === 0) {
            return;
          }
          this.currentIndex = -1;
          this.datalistItemElementsCreation(states);
        });
    }
  }

  inputClearIconElementCreation() {
    const element = document.createElement('i');
    element.classList.add('autocomplete-clear');
    element.addEventListener('click', () => this.inputClearIconClickCallback());
    return element;
  }
  inputClearIconClickCallback() {
    this.datalistCleanOptions();
    this.dispatchCompleteEvent({});
    this.autocompleteWrapperEl.classList.remove('not-empty');
    this.autocompleteInputEl.value = '';
    this.autocompleteInputEl.focus();
  }
  //#endregion

  //#region Datalist Element Creation
  datalistElementCreation() {
    const element = document.createElement('ul');
    element.classList.add('autocomplete-datalist', 'hide');
    return element;
  }
  datalistCleanOptions() {
    this.autocompleteDatalistEl.innerHTML = '';
    this.autocompleteDatalistEl.classList.remove('show');
    this.autocompleteDatalistEl.classList.add('hide');
    this.datalistItemCleanAllHover();
  }

  datalistItemElementsCreation(states) {
    this.autocompleteDatalistEl.classList.add('show');
    this.autocompleteDatalistEl.classList.remove('hide');
    this.states = [];
    states.data.forEach(state => {
      const element = document.createElement('li');
      element.setAttribute('id', `${state.abbreviation}.${state.name}`)
      element.addEventListener('click', () => this.datalistItemOnClickCallback(state));
      element.innerHTML = state.name;
      this.autocompleteDatalistEl.appendChild(element);
      this.states.push(state);
    });
  }
  datalistItemOnClickCallback($event) {
    this.datalistCleanOptions();
    this.dispatchCompleteEvent($event);
    this.autocompleteInputEl.value = $event.name;
    this.autocompleteInputEl.blur();
  }
  datalistItemCleanAllHover() {
    for (let i = 0; i < this.autocompleteDatalistEl.children.length; i++) {
      const stateElement = this.autocompleteDatalistEl.children[i];
      stateElement.classList.remove('hover');
    }
  }
  //#endregion

  dispatchCompleteEvent($event) {
    console.log('[DEV] | dispatch: ', $event);
    this.selected = $event;
    const changeEventCallback = new CustomEvent('complete', {
      bubbles: true,
      cancelable: false,
      detail: $event
    });
    this.dispatchEvent(changeEventCallback);
  }

}

module.exports = AutocompleteComponent;
customElements.define('v-autocomplete', AutocompleteComponent);
