'use strict';

const express = require('express');
const routes = require('./routes/index');
const states = require('./api/states');

const app = express();

app.use('/dist', express.static('dist'));
app.set('port', 3000);

app.listen(app.get('port'));

app.use('/', routes);
app.use('/api/states', states);
