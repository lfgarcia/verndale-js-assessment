# Javascript Assessment
### _**Author**: Luis García Castro_
### _**Date**: 20/Jun/2019_

# Environment Preparation
1. sudo npm install -g webpack@3.10.0

1. npm install

1. webpack

1. Go to: http://localhost:3000

# Showcase

![Empty](./documentation/01_empty.png)

![No State](./documentation/02_no_state.png)

![Searching](./documentation/03_searching.png)

![Hover and Arrow Nav](./documentation/04_hover_arrow_nav.png)

![Selected](./documentation/05_selected.png)

